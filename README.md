# Xavey Form Builder Library

## Library files
1. js/formbuilder.js
2. css/formbuilder.css

## Demo files
1. index.html
2. preview.html
3. css/style.css
4. audio/audio.mp3
5. video/video.mp4
6. img/stars.svg
7. icons/files

## To do
1. skipTo (both input, choice)
2. RatingElement (square and dot svg image)
3. GroupElement
4. ParseFromJSON

## Classes

### FormBuilder

FormBuilder class is the base class of the form building process. It contains list of form elements, parse JSON to form elements, generate JSON from form elements and render survey wizard.

FormBuilder also contains HTML DOM elements for rendering.

### FormElement
###### abstract

FormElement is the parent class of all form element types. It has all basic functionalities.

FormElement also contains HTML DOM elements for rendering question and answers. Different FormElement has different HTML DOM elements for different purposes.

### TextElement
###### extends FormElement
###### abstract

TextElement is the parent class of all text form element types. It has all basic text related functionalities.

### ShortTextElement
###### extends TextElement

ShortTextElement is to receive one line text input. It generates HTML version of `<input type='text'>` element.

It can have minimum and maximum character length.

### LongTextElement
###### extends TextElement

LongTextElement is to receive one or more line text input. It generates HTML version of `<textarea type='text'></textarea>` element.

It can have minimum and maximum character length.

### NumberElement
###### extends TextElement

NumberElement is to receive number input. It generates HTML version of `<input type='number'>` element.

It can have minimum and maximum number value.

### PercentageElement
###### extends NumberElement

PercentageElement is to receive percentage number input. It generates HTML version of `<input type='number'>` element.

Its default minimum number value is 0 and maximum number value is 100.

It has extra `span` HTML DOM element to display `%` percentage sign.

### CurrencyElement
###### extends NumberElement

CurrencyElement is to receive currency number input. It generates HTML version of `<input type='number'>` element.

Its default currency is dollar `$`.

It has extra `span` HTML DOM element to display currency sign.

### RatingElement
###### extends FormElement

RatingElement is to receive rating input. It generates custom HTML DOM elements.

Its default step size is 5 and default rating is 0. Its default rating shape is star.

Available rating shapes are star, square and dot.

### ChoiceElement
###### extends FormElement
###### abstract

ChoiceElement is the parent class of all multiple choice form element types.

It contains Choice objects. Answer can be single or multiple. Single answer will be asked by `Radio Buttons` and multiple answers will be asked by `CheckBoxes`.

### Choice

Choice is the class to contain pre-recorded answer for multiple choice question. It is contained in ChoiceElement.

Choice can be text, image, audio or video. It can have skip_to value.

### DropdownElement
###### extends ChoiceElement

DropdownElement is to receive single or multiple choice of text answers. It generates HTML version of `<select><option>...</option></select>` element. 

It can be single or multiple choice.

### CheckBoxElement
###### extends ChoiceElement

CheckBoxElement is to receive multiple choice of text answers. It generates HTML version of `<input type='checkbox'>` elements.

### RadioElement
###### extends ChoiceElement

RadioElement is to receive single choice of text answer. It generates HTML version of `<input type='radio'>` elements.

### ImageSelectElement
###### extends ChoiceElement

ImageSelect is to receive single or multiple choice of image answer. It generates custom HTML elements.

Answer can be single or multiple. Single answer will be asked by `Radio Buttons` and multiple answers will be asked by `CheckBoxes`.

### AudioSelectElement
###### extends ChoiceElement

AudioSelect is to receive single or multiple choice of audio answer. It generates custom HTML elements.

Answer can be single or multiple. Single answer will be asked by `Radio Buttons` and multiple answers will be asked by `CheckBoxes`.

### VideoSelectElement
###### extends ChoiceElement

VideoSelect is to receive single or multiple choice of video answer. It generates custom HTML elements.

Answer can be single or multiple. Single answer will be asked by `Radio Buttons` and multiple answers will be asked by `CheckBoxes`.
