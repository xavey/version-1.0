/*
 * FormBuilder JavaScript Library
 * 
 * Company: Xavey
 * Developer: Comquas
 * Created Date: 09 Oct 2017
 * Modified Date: 19 Oct 2017
 *
 * TODO:
 * - RatingElement (square and dot)
 * - GroupElement (Need to double-check)
 * - ParseFromJSON
 */

'use strict'

/*
 * getTodayDate()
 * Return today date in dd/mm/yyyy format
 */
function getTodayDate() {
	let today = new Date();
	let date = today.getDate();
	let month = today.getMonth() + 1;
	let year = today.getFullYear();

	return ((date < 10) ? '0' + date : date) + '/' + ((month < 10) ? '0' + month : month) + '/' + year;
}

/*
 * FormBuilder
 * Form builder class to parse json to form, for to generate json or build form
 * It contains form elements
 */
class FormBuilder {
	constructor(json) {
		this.json = json;

		this.projectName = "Untitle Project";
		this.createdDate = getTodayDate();
		this.modifiedDate = getTodayDate();
		this.formElements = [];
		this.document = {};
		this.step = 0;

		if(json) {
			// TODO: parse json
		}
	}

	/*
	 * setProjectName(projectName)
	 * @projectName - project name of form builder
	 */
	setProjectName(projectName) {
		this.projectName = projectName;
	}

	/*
	 * setCreateDate(createdDate)
	 * @createdDate - created date of form builder
	 */
	setCreatedDate(createdDate) {
		this.createdDate = createdDate;
	}

	/*
	 * setModifiedDate(modifiedDate)
	 * @modifiedDate - modified date of form builder
	 */
	setModifiedDate(modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/*
	 * setFormElements(formElements)
	 * @formElements - form elements to add
	 */
	setFormElements(formElements) {
		this.formElements = formElements;
	}

	/*
	 * addFormElement(formElement, index)
	 * @formElement - form element to add
	 * @index - index to add form element
	 */
	addFormElement(formElement, index) {
		if(index)
			this.formElements.splice(index, 0, formElement);
		else
			this.formElements.push(formElement);
	}

	/*
	 * removeFormElement(index)
	 * @index - remove form element at index
	 */
	removeFormElement(index) {
		if(index)
			this.formElements.splice(idnex, 1);
	}

	/*
	 * RenderFlat
	 * Render all form elements in a flat document
	 * @parent: parent element
	 */
	renderFlat(parent) {
		this.formElements.forEach(element => {
			element.render(parent);
		});
	}

	/*
	 * Render
	 * Render all from elements in wizard.
	 * @parent: parent element
	 */
	render(parent) {
		this.step = 0;

		this.document.wizard = document.createElement('div');
		this.document.wizard.className = 'wizard';

		this.document.step = document.createElement('div');
		this.document.step.className = 'step';
		this.document.step.innerHTML = `${this.step + 1}.`;

		this.document.panel = document.createElement('div');
		this.document.panel.className = 'panel';

		this.document.previous = document.createElement('button');
		this.document.previous.className = 'button-default button-previous';
		this.document.previous.type = 'button';
		this.document.previous.id = 'button-previous';
		this.document.previous.innerHTML = 'Previous';
		this.document.previous.style.display = 'none';

		this.document.next = document.createElement('button');
		this.document.next.className = 'button-primary button-next';
		this.document.next.type = 'button';
		this.document.next.id = 'button-next';
		this.document.next.innerHTML = 'Next';

		this.document.submit = document.createElement('button');
		this.document.submit.className = 'button-primary button-submit';
		this.document.submit.type = 'submit';
		this.document.submit.id = 'button-submit';
		this.document.submit.innerHTML = 'Submit';
		this.document.submit.style.display = 'none';

		this.document.panel.appendChild(this.formElements[0].document.form);
		this.document.wizard.appendChild(this.document.step);
		this.document.wizard.appendChild(this.document.panel);
		this.document.wizard.appendChild(this.document.previous);
		this.document.wizard.appendChild(this.document.next);
		this.document.wizard.appendChild(this.document.submit);

		this.document.previous.addEventListener('click', event => {
			let currentFormElement = this.formElements[this.step];

			if(currentFormElement) {
				let previousFormElement = this.formElements[currentFormElement.getComeFrom()];

				if(previousFormElement) {
					this.step = currentFormElement.getComeFrom();

					this.document.step.innerHTML = `${this.step + 1}.`;
					this.document.panel.innerHTML = '';
					this.document.panel.appendChild(previousFormElement.document.form);
				}

				if(this.step == 0) {
					this.document.previous.style.display = 'none';
				}

				if(this.step < this.formElements.length - 1) {
					this.document.submit.style.display = 'none';
					this.document.next.style.display = 'inline-block';
				}

				if(this.step > 0) {
					this.document.previous.style.display = 'inline-block';
				} 
			}
		});

		this.document.next.addEventListener('click', event => {
			let currentFormElement = this.formElements[this.step];

			if(currentFormElement) {

				// if it is required element, check if it has answer or not based on element type
				switch(currentFormElement.constructor.name) {
					case 'ShortTextElement':
					case 'LongTextElement':
					case 'NumberElement':
					case 'PercentageElement':
					case 'CurrencyElement': {
						if(currentFormElement.getRequired()) {
							let answer = currentFormElement.getAnswer();
							if(answer.length < 1) {
								alert("Please enter the answer.");
								return;
							}
						}
					} break;
					case 'RatingElement': {
						if(currentFormElement.getRequired()) {
							let answer = currentFormElement.getAnswer();
							if(answer < 1) {
								alert("Please select the rating.");
								return;
							}
						}
					} break;
					case 'DropdownElement':
					case 'CheckBoxElement':
					case 'RadioElement':
					case 'ImageSelectElement':
					case 'AudioSelectElement':
					case 'VideoSelectElement': {
						if(currentFormElement.getRequired()) {
							let answers = currentFormElement.getAnswers();
							if(answers.length < 1) {
								alert("Please select the answer.");
								return;
							}
						}
					}
				}

				/* skipTo priority = smallest choice skipTo > choice skipTo > current element skipTo
				 * if the current form element is single or multiple choice element,
				 * the smallest choice skipTo will be used.
				 * if both current form element and its selected choices have skipTo,
				 * the choice skipTo will be used.
				 */
				if(currentFormElement.getSkipTo() > 0) {

					let nextFormElement;

					// if current form element has skipTo, form will skip to skipTo
					// otherwise form will go to next step.
					switch(currentFormElement.constructor.name) {
						case 'ShortTextElement':
						case 'LongTextElement':
						case 'NumberElement':
						case 'PercentageElement':
						case 'CurrencyElement': {
							let answer = currentFormElement.getAnswer();
							if(answer.length > 0) {
								nextFormElement = this.formElements[currentFormElement.getSkipTo()];
								nextFormElement.setComeFrom(this.step);
								this.step = currentFormElement.getSkipTo();
							}
							else {
								nextFormElement = this.formElements[this.step + 1];
								nextFormElement.setComeFrom(this.step);
								this.step = this.step + 1;
							}
						} break;
						case 'RatingElement': {
							let answer = currentFormElement.getAnswer();
							if(answer > 0) {
								nextFormElement = this.formElements[currentFormElement.getSkipTo()];
								nextFormElement.setComeFrom(this.step);
								this.step = currentFormElement.getSkipTo();
							}
							else {
								nextFormElement = this.formElements[this.step + 1];
								nextFormElement.setComeFrom(this.step);
								this.step = this.step + 1;
							}
						} break;
						case 'DropdownElement':
						case 'CheckBoxElement':
						case 'RadioElement':
						case 'ImageSelectElement':
						case 'AudioSelectElement':
						case 'VideoSelectElement': {
							let selectedItems = currentFormElement.getSelectedItems();
							if(selectedItems.length > 0) {
								let choiceSkipTo = [];

								for(let i = 0; i < selectedItems.length; i++) {
									let item = selectedItems[i];
									let choice = currentFormElement.getChoice(parseInt(item.getAttribute('data-index')));

									if(choice.getSkipTo()) {
										// get selected choice by selected item data-index
										choiceSkipTo.push(choice);
									}
								}

								if(choiceSkipTo.length > 0) {
									// sort in accending order to get smaller skipTo at index 0
									choiceSkipTo.sort(function(a, b){return a.getSkipTo()-b.getSkipTo()});
									nextFormElement = this.formElements[choiceSkipTo[0].getSkipTo()];
									nextFormElement.setComeFrom(this.step);
									this.step = choiceSkipTo[0].getSkipTo();
								}
								else {
									nextFormElement = this.formElements[currentFormElement.getSkipTo()];
									nextFormElement.setComeFrom(this.step);
									this.step = currentFormElement.getSkipTo();
								}
							}
							else {
								nextFormElement = this.formElements[this.step + 1];
								nextFormElement.setComeFrom(this.step);
								this.step = this.step + 1;
							}
						}
					}

					if(nextFormElement) {
						this.document.step.innerHTML = `${this.step + 1}.`;
						this.document.panel.innerHTML = '';
						this.document.panel.appendChild(nextFormElement.document.form);
					}
				}
				else {
					let nextFormElement;

					// although current form element has no skipTp, if the element type if single or multiple choice,
					// its answers will still be checked for skipTo.
					switch(currentFormElement.constructor.name) {
						case 'ShortTextElement':
						case 'LongTextElement':
						case 'NumberElement':
						case 'PercentageElement':
						case 'CurrencyElement': 
						case 'RatingElement': {
							// not single or multiple choice element, straight to next step.
							nextFormElement = this.formElements[this.step + 1];
							nextFormElement.setComeFrom(this.step);
							this.step = this.step + 1;
						} break;
						case 'DropdownElement':
						case 'CheckBoxElement':
						case 'RadioElement':
						case 'ImageSelectElement':
						case 'AudioSelectElement':
						case 'VideoSelectElement': {
							// single or multiple choice element, need to check its answers skipTo.
							let selectedItems = currentFormElement.getSelectedItems();
							let choiceSkipTo = [];

							for(let i = 0; i < selectedItems.length; i++) {
								let item = selectedItems[i];
								let choice = currentFormElement.getChoice(parseInt(item.getAttribute('data-index')));

								if(choice.getSkipTo()) {
									// get selected choice by selected item data-index
									choiceSkipTo.push(choice);
								}
							}

							if(choiceSkipTo.length > 0) {
								// sort in accending order to get smaller skipTo at index 0
								choiceSkipTo.sort(function(a, b){return a.getSkipTo()-b.getSkipTo()});
								nextFormElement = this.formElements[choiceSkipTo[0].getSkipTo()];
								nextFormElement.setComeFrom(this.step);
								this.step = choiceSkipTo[0].getSkipTo();
							}
							else {
								nextFormElement = this.formElements[this.step + 1];
								nextFormElement.setComeFrom(this.step);
								this.step = this.step + 1;
							}
						}
					}
					
					if(nextFormElement) {
						this.document.step.innerHTML = `${this.step + 1}.`;
						this.document.panel.innerHTML = '';
						this.document.panel.appendChild(nextFormElement.document.form);
					}
				}

				if(this.step == this.formElements.length - 1) {
					this.document.next.style.display = 'none';
					this.document.submit.style.display = 'inline-block';
				}

				if(this.step > 0) {
					this.document.previous.style.display = 'inline-block';
				} 
			}
		});

		this.document.submit.addEventListener('click', event => {
			let currentFormElement = this.formElements[this.step];

			if(currentFormElement) {
				switch(currentFormElement.constructor.name) {
					case 'ShortTextElement':
					case 'LongTextElement':
					case 'NumberElement':
					case 'PercentageElement':
					case 'CurrencyElement': {
						let answer = currentFormElement.getAnswer();
						if(answer.length < 1) {
							alert("Please enter the answer.");
							return;
						}
					} break;
					case 'RatingElement': {
						let answer = currentFormElement.getAnswer();
						if(answer < 1) {
							alert("Please select the rating.");
							return;
						}
					} break;
					case 'DropdownElement':
					case 'CheckBoxElement':
					case 'RadioElement':
					case 'ImageSelectElement':
					case 'AudioSelectElement':
					case 'VideoSelectElement': {
						let answers = currentFormElement.getAnswers();
						if(answers.length < 1) {
							alert("Please select the answer.");
							return;
						}
					}
				}
			}

			this.toJSON();
		});

		parent.appendChild(this.document.wizard);
	}

	toJSON() {
		let json = {};

		json.project = {};
		json.project.name = this.projectName;
		json.project.createdDate = this.createdDate;
		json.project.modifiedDate = this.modifiedDate;
		json.form = [];

		for(let i = 0; i < this.formElements.length; i++) {
			let formElement = this.formElements[i];
			json.form.push(formElement.toJSON());
		}

		console.log(json);
		console.log(JSON.stringify(json));

		return JSON.stringify(json);
	}
}

class FormElement {
	constructor() {
		if(new.target === FormElement) {
			throw new TypeError(`Error: FormElement is abstract.`);
		}

		// id is form id of the form element
		this.id = null;
		// name is form name of the form element
		this.name = null;
		this.type = null;
		this.question = null;
		this.description = null;
		this.media = {};
		this.settings = {};
		this.settings.required = false;
		this.media.imageURL = null;
		this.media.videoURL = null;
		this.media.audioURL = null;
		this.skipTo = null;
		this.comeFrom = null;

		// this.document contains DOM elements
		this.document = {};
		this.document.form = document.createElement('div');
		this.document.form.className = 'form-element';
		this.document.question = document.createElement('div');
		this.document.question.className = 'question';
		this.document.answer = document.createElement('div');
		this.document.answer.className = 'answer';

		// every form element contains question and answer
		this.document.form.appendChild(this.document.question);
		this.document.form.appendChild(this.document.answer);
	}

	setId(id) {
		this.id = id;
		this.document.form.setAttribute('id', id);
	}

	setName(name) {
		this.name = name;
	}

	setType(type) {
		this.type = type;
	}

	setQuestion(question) {
		this.question = question;
		this.document.question.innerHTML = question;
	}

	setDescription(description) {
		this.description = description;
	}

	setMedia(media) {
		this.media = media;
	}

	setImageURL(imageURL) {
		this.media.imageURL = imageURL;

		if(this.media.imageURL) {
			this.document.image = document.createElement('img');
			this.document.image.className = 'question-image';
			this.document.image.setAttribute('src', this.media.imageURL);
			this.document.question.appendChild(this.document.image);
		}
	}

	setVideoURL(videoURL) {
		this.media.videoURL = videoURL;

		if(this.media.videoURL) {
			this.document.video = document.createElement('video');
			this.document.video.className = 'question-video';
			this.document.video.setAttribute('src', this.media.videoURL);
			this.document.video.setAttribute('controls', 'controls')
			this.document.question.appendChild(this.document.video);
		}
	}

	setAudioURL(audioURL) {
		this.media.audioURL = audioURL;

		if(this.media.audioURL) {
			this.document.audio = document.createElement('audio');
			this.document.audio.className = 'question-audio';
			this.document.audio.setAttribute('src', this.media.audioURL);
			this.document.audio.setAttribute('controls', 'controls')
			this.document.question.appendChild(this.document.audio);
		}
	}

	setSettings(settings) {
		this.settings = settings;
	}

	setRequired(required) {
		this.settings.required = required;
	}

	setSkipTo(skipTo) {
		this.skipTo = skipTo;
	}

	setComeFrom(comeFrom) {
		this.comeFrom = comeFrom;
	}

	getId() {
		return this.id;
	}

	getName() {
		return this.name;
	}

	getType() {
		return this.type;
	}

	getQuestion() {
		return this.question;
	}

	getDescription() {
		return this.description;
	}

	getMedia() {
		return this.media;
	}

	getImageURL() {
		return this.media.imageURL;
	}

	getVideoURL() {
		return this.media.videoURL;
	}

	getAudioURL() {
		return this.media.audioURL;
	}

	getSettings() {
		return this.settings;
	}

	getRequired() {
		return this.settings.required;
	}

	getSkipTo() {
		return this.skipTo;
	}

	getComeFrom() {
		return this.comeFrom;
	}

	toJSON() {
		return {};
	}
}

class TextElement extends FormElement {
	constructor() {
		super();
		
		if(new.target === TextElement) {
			throw new TypeError(`Error: TextElement is abstract.`);
		}

		this.settings.multiLine = false;
		this.settings.maxChar = null;
		this.settings.maxNumber = null;
		this.settings.minNumber = null;
	}

	setMultiLine(multiLine) {
		this.settings.multiLine = multiLine;
	}

	setMaxChar(maxChar) {
		this.settings.maxChar = maxChar;
	}

	setMaxNumber(maxNumber) {
		this.settings.maxNumber = maxNumber;
	}

	setMinNumber(minNumber) {
		this.settings.minNumber = minNumber;
	}

	getMultiLine() {
		return this.settings.multiLine;
	}

	getMaxChar() {
		return this.settings.maxChar;
	}

	getMaxNumber() {
		return this.settings.maxNumber;
	}

	getMinNumber() {
		return this.settings.minNumber;
	}

	getAnswer() {
		return this.document.input.value;
	}

	toJSON() {
		let json = {};

		json.id = this.getId();
		json.name = this.getName();
		json.type = this.getType();
		json.question = this.getQuestion();
		json.description = this.getDescription();
		json.media = this.getMedia();
		json.settings = this.getSettings();

		if(this.skipTo)
			json.skip_to = this.getSkipTo();

		json.answer = this.getAnswer();

		return json;
	}
}

class ShortTextElement extends TextElement {
	constructor() {
		super();

		this.type = 'short-text';
		this.settings.multiLine = false;

		this.document.input = document.createElement('input');
		this.document.input.type = 'text';
		this.document.input.className = 'form-input';

		this.document.answer.appendChild(this.document.input);

		this.document.input.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setMaxChar(maxChar) {
		super.setMaxChar(maxChar);
		this.document.input.setAttribute('maxlength', maxChar);
	}

	getAnswer() {
		return this.document.input.value;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class LongTextElement extends TextElement {
	constructor() {
		super();

		this.type = 'long-text';
		this.settings.multiLine = true;

		this.document.input = document.createElement('textarea');
		this.document.input.setAttribute('rows', '3');
		this.document.input.className = 'form-input';

		this.document.answer.appendChild(this.document.input);

		this.document.input.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setMaxChar(maxChar) {
		super.setMaxChar(maxChar);
		this.document.input.setAttribute('maxlength', maxChar);
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class NumberElement extends TextElement {	
	constructor() {
		super();

		this.type = 'number';
		this.settings.multiLine = false;
		
		this.document.input = document.createElement('input');
		this.document.input.type = 'number';
		this.document.input.className = 'form-input';

		this.document.answer.appendChild(this.document.input);

		this.document.input.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setMinNumber(minNumber) {
		super.setMinNumber(minNumber);
		this.document.input.setAttribute('min', minNumber);
	}

	setMaxNumber(maxNumber) {
		super.setMaxNumber(maxNumber);
		this.document.input.setAttribute('max', maxNumber);
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class PercentageElement extends NumberElement {	
	constructor() {
		super();

		this.type = 'percentage';

		this.document.input.className = this.document.input.className + ' form-percentage';
		this.document.span = document.createElement('span');
		this.document.span.className = 'percentage';
		this.document.span.innerHTML = '%';
		this.document.answer.appendChild(this.document.span);
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class CurrencyElement extends NumberElement {	
	constructor() {
		super();

		this.type = 'currency';
		// $ is the default currency type
		this.currency = '$';

		this.document.input.className = this.document.input.className + ' form-currency';
		this.document.span = document.createElement('span');
		this.document.span.className = 'currency';
		this.document.span.innerHTML = '$';
		this.document.answer.appendChild(this.document.span);
	}

	/* 
	 * setCurrency(currency)
	 * @currency - type of the currency
	 */
	setCurrency(currency) {
		this.currency = currency;
		this.document.span.innerHTML = currency;
	}

	/* getCurrency()
	 * Return currency type
	 */
	getCurrency() {
		return this.currency;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class RatingElement extends FormElement {
	constructor() {
		super();

		this.type = 'rating';

		this.steps = [];
		this.shape = "star";
		// shape types
		this.availableShapes = ["star", "square", "dot"];
		this.rating = 0;
		
		this.document.answer.className = 'answer rating';
	}

	setSteps(steps) {
		this.document.answer.innerHTML = '';
		for(let i = 0; i < steps; i++) {
			let step = document.createElement('div');
			step.className = 'rating-step ' + this.shape;
			step.setAttribute('data-index', i);
			this.steps.push(step);
			this.document.answer.appendChild(step);

			step.addEventListener('mouseover', event => {
				for(let i = 0; i < this.steps.length; i++) {
					let item = this.steps[i];

					if (i <= parseInt(step.getAttribute('data-index'))) {
					  item.classList.add('active');
					} else {
					  item.classList.remove('active');
					}
				}
			});

			step.addEventListener('mouseout', event => {
				for(let i = 0; i < this.steps.length; i++) {
					let item = this.steps[i];

					if (this.rating >= (parseInt(item.getAttribute('data-index')) + 1)) {
					  item.classList.add('active');
					} else {
					  item.classList.remove('active');
					}
				}
			});

			step.addEventListener('click', event => {
				this.setRating(parseInt(step.getAttribute('data-index')) + 1);
			});
		}
	}

	setShape(shape) {
		this.shape = shape;
		for(let i = 0; i < this.steps.length; i++) {
			let item = this.steps[i];
			item.className = 'rating-step ' + this.shape;	
		}

		this.setRating(this.rating);
	}

	setRating(rating) {
		this.rating = rating;
		for(let i = 0; i < this.steps.length; i++) {
			let item = this.steps[i];
			if(this.rating >= (parseInt(item.getAttribute('data-index')) + 1)) {
				item.classList.add('active');
			}
			else {
				item.classList.remove('active');
			}
		}
	}

	getSteps() {
		return this.steps.length;
	}

	getShape() {
		return this.shape;
	}

	getAvailableShapes() {
		return this.availableShapes;
	}

	getAnswer() {
		return this.rating;	
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}

	toJSON() {
		let json = {};

		json.id = this.getId();
		json.name = this.getName();
		json.type = this.getType();
		json.question = this.getQuestion();
		json.description = this.getDescription();
		json.media = this.getMedia();
		json.settings = this.getSettings();

		if(this.skipTo)
			json.skip_to = this.getSkipTo();

		json.answer = this.getAnswer();

		return json;
	}
}

class ChoiceElement extends FormElement {
	constructor() {
		super();

		if(new.target === ChoiceElement) {
			throw new TypeError(`Error: ChoiceElement is abstract.`);
		}

		this.choices = [];
		this.settings.multiAnswer = false;
	}

	setChoices(choices) {
		this.choices = choices;
	}

	setMultiAnswer(multiAnswer) {
		this.settings.multiAnswer = multiAnswer;
	}

	getChoices() {
		return this.choices;
	}

	getChoice(index) {
		return this.choices[index];
	}

	getMultiAnswer() {
		return this.settings.multiAnswer;
	}

	addChoice(choice) {
		this.choices.push(choice);
	}

	removeChoice(index) {
		this.choices.splice(index, 1);
	}

	toJSON() {
		let json = {};

		json.id = this.getId();
		json.name = this.getName();
		json.type = this.getType();
		json.question = this.getQuestion();
		json.description = this.getDescription();
		json.media = this.getMedia();
		json.settings = this.getSettings();

		if(this.skipTo)
			json.skip_to = this.getSkipTo();

		json.answers = this.getAnswers();

		json.choices = [];

		for(let i = 0; i < this.choices.length; i++) {
			let item = this.choices[i];
			let choice = {};

			choice.id = item.getId();
			choice.text = item.getText();
			choice.media = item.getMedia();

			if(item.getSkipTo())
				choice.skip_to = item.getSkipTo();

			json.choices.push(choice);
		}

		return json;
	}
}

class Choice {
	constructor() {
		this.id = null;
		this.text = "";
		this.media = {};
		this.media.imageURL = null;
		this.media.audioURL = null;
		this.media.videoURL = null;
		this.skipTo = null;
	}

	setId(id) {
		this.id = id;
	}

	setText(text) {
		this.text = text;
	}

	setMedia(media) {
		this.media = media;
	}

	setImageURL(imageURL) {
		this.media.imageURL = imageURL;
	}

	setAudioURL(audioURL) {
		this.media.audioURL = audioURL;
	}

	setVideoURL(videoURL) {
		this.media.videoURL = videoURL;
	}

	setSkipTo(skipTo) {
		this.skipTo = skipTo;
	}

	getId() {
		return this.id;
	}

	getText() {
		return this.text;
	}

	getMedia() {
		return this.media;
	}

	getImageURL() {
		return this.media.imageURL;
	}

	getAudioURL() {
		return this.media.audioURL;
	}

	getVideoURL() {
		return this.media.videoURL;
	}

	getSkipTo() {
		return this.skipTo;
	}
}

class DropdownElement extends ChoiceElement {
	constructor() {
		super();

		this.type = 'dropdown';

		this.document.select = document.createElement('select');
		this.document.select.className = 'form-input';
		this.document.options = [];

		this.document.answer.appendChild(this.document.select);

		this.document.answer.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setChoices(choices) {
		super.setChoices(choices);

		this.document.options = [];

		for(let i = 0; i < this.document.select.options.length; i++) {
			this.document.select.remove(i);
		}

		for(let i = 0; i < choices.length; i++) {
			let choice = choices[i];
			let option = document.createElement('option');
			option.value = choice.getId();
			option.innerHTML = choice.getText();
			option.setAttribute('data-index', i);

			this.document.options.push(option);
			this.document.select.add(option);
		}
	}

	setMultiAnswer(multiAnswer) {
		super.setMultiAnswer(multiAnswer);

		if(multiAnswer)
			this.document.select.setAttribute('multiple', 'multiple');
		else
			this.document.select.removeAttribute('multiple');
	}

	addChoice(choice) {
		super.addChoice(choice);

		let option = document.createElement('option');
		option.value = choice.getId();
		option.innerHTML = choice.getText();
		option.setAttribue('data-index', this.choices.length);

		this.document.options.push(option);
		this.document.select.add(option);
	}

	removeChoice(index) {
		super.removeChoice(index);

		this.document.options.splice(index, 1);
		this.document.select.remove(index);

		// resort data-index
		for(var i = 0; i < this.document.options.length; i++) {
			let option = this.document.options[i];
			option.setAttribute('data-index', i);
		}
	}

	getSelectedItems() {
		let items = [];

		for(var i = 0; i < this.document.select.options.length; i++) {
			let option = this.document.select.options[i];

			if(option.selected) {
				items.push(option);
			}
		}

		return items;
	}

	getAnswers() {
		let answers = [];

		for(var i = 0; i < this.document.select.options.length; i++) {
			let option = this.document.select.options[i];

			if(option.selected) {
				answers.push(option.value);
			}
		}

		return answers;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class CheckBoxElement extends ChoiceElement {
	constructor() {
		super();

		this.type = 'checkbox';
		this.settings.multiAnswer = true;

		this.document.div = document.createElement('div');
		this.document.div.className = 'form-input';
		this.document.options = [];

		this.document.answer.appendChild(this.document.div);

		this.document.answer.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setChoices(choices) {
		super.setChoices(choices);

		this.document.options = [];
		this.document.div.innerHTML = '';

		for(let i = 0; i < choices.length; i++) {
			let choice = choices[i];
			let option = document.createElement('label');
			let input = document.createElement('input')
			input.name = this.name;
			input.value = choice.getId();
			input.type = 'checkbox';

			option.appendChild(input);
			option.innerHTML += ' ' + choice.getText();
			option.setAttribute('data-index', i);

			this.document.options.push(option);
			this.document.div.appendChild(option);
		}
	}

	addChoice(choice) {
		super.addChoice(choice);

		let choice = choices[i];
		let option = document.createElement('label');
		let input = document.createElement('input')
		input.name = this.name;
		input.value = choice.getId();
		input.type = 'checkbox';

		option.appendChild(input);
		option.innerHTML += ' ' + choice.getText();
		option.setAttribute('data-index', i);

		this.document.options.push(option);
		this.document.div.appendChild(option);
	}

	removeChoice(index) {
		super.removeChoice(index);

		this.document.options.splice(index, 1);
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let data_index = parseInt(option.getAttribute('data-index'));

			if(index == data_index) {
				option.remove();
			}
		}

		// resort data-index
		options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			option.setAttribute('data-index', i);
		}
	}

	getSelectedItems() {
		let items = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				items.push(option);
			}
		}

		return items;
	}

	getAnswers() {
		let answers = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				answers.push(input.value);
			}
		}

		return answers;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class RadioElement extends ChoiceElement {
	constructor() {
		super();

		this.type = 'radio';
		this.settings.multiAnswer = false;

		this.document.div = document.createElement('div');
		this.document.div.className = 'form-input';
		this.document.options = [];

		this.document.answer.appendChild(this.document.div);

		this.document.answer.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setChoices(choices) {
		super.setChoices(choices);

		this.document.options = [];
		this.document.div.innerHTML = '';

		for(let i = 0; i < choices.length; i++) {
			let choice = choices[i];
			let option = document.createElement('label');
			let input = document.createElement('input')
			input.name = this.name;
			input.value = choice.getId();
			input.type = 'radio';

			option.appendChild(input);
			option.innerHTML += ' ' + choice.getText();
			option.setAttribute('data-index', i);

			this.document.options.push(option);
			this.document.div.appendChild(option);
		}
	}

	addChoice(choice) {
		super.addChoice(choice);

		let choice = choices[i];
		let option = document.createElement('label');
		let input = document.createElement('input')
		input.name = this.name;
		input.value = choice.getId();
		input.type = 'checkbox';

		option.appendChild(input);
		option.innerHTML += ' ' + choice.getText();
		option.setAttribute('data-index', i);

		this.document.options.push(option);
		this.document.div.appendChild(option);
	}

	removeChoice(index) {
		super.removeChoice(index);

		this.document.options.splice(index, 1);
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let data_index = parseInt(option.getAttribute('data-index'));

			if(index == data_index) {
				option.remove();
			}
		}

		// resort data-index
		options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			option.setAttribute('data-index', i);
		}
	}

	getSelectedItems() {
		let items = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				items.push(option);
			}
		}

		return items;
	}

	getAnswers() {
		let answers = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				answers.push(input.value);
			}
		}

		return answers;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class ImageSelectElement extends ChoiceElement {
	constructor() {
		super();

		this.type = 'image-select';
		this.settings.multiAnswer = false;

		this.document.form.classList += ' form-image-select';
		this.document.div = document.createElement('div');
		this.document.div.className = 'form-input';
		this.document.options = [];

		this.document.answer.appendChild(this.document.div);

		this.document.answer.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setChoices(choices) {
		super.setChoices(choices);

		this.document.options = [];
		this.document.div.innerHTML = '';

		for(let i = 0; i < choices.length; i++) {
			let choice = choices[i];
			let option = document.createElement('label');
			let input = document.createElement('input');
			let image = document.createElement('img');

			input.name = this.name;
			input.value = choice.getId();

			if(this.settings.multiAnswer)
				input.type = 'checkbox';
			else
				input.type = 'radio';

			image.setAttribute('src', choice.getImageURL());
			option.appendChild(input);
			option.appendChild(image);
			option.setAttribute('data-index', i);

			this.document.options.push(option);
			this.document.div.appendChild(option);
		}
	}

	setMultiAnswer(multiAnswer) {
		if(this.settings.multiAnswer != multiAnswer) {
			super.setMultiAnswer(multiAnswer);

			for(var i = 0; i < this.document.options.length; i++) {
				let option = this.document.options[i];
				let input = option.firstChild;

				if(this.settings.multiAnswer)
					input.type = 'checkbox';
				else
					input.type = 'radio';
			}
		}
	}

	addChoice(choice) {
		super.addChoice(choice);

		let choice = choices[i];
		let option = document.createElement('label');
		let input = document.createElement('input');
		let image = document.createElement('img');

		input.name = this.name;
		input.value = choice.getId();

		if(this.settings.multiAnswer)
			input.type = 'checkbox';
		else
			input.type = 'radio';

		image.setAttribute('src', choice.getImageURL());
		option.appendChild(input);
		option.appendChild(image);
		option.setAttribute('data-index', i);

		this.document.options.push(option);
		this.document.div.appendChild(option);
	}

	removeChoice(index) {
		super.removeChoice(index);

		this.document.options.splice(index, 1);
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let data_index = parseInt(option.getAttribute('data-index'));

			if(index == data_index) {
				option.remove();
			}
		}

		// resort data-index
		options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			option.setAttribute('data-index', i);
		}
	}

	getSelectedItems() {
		let items = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				items.push(option);
			}
		}

		return items;
	}

	getAnswers() {
		let answers = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				answers.push(input.value);
			}
		}

		return answers;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class AudioSelectElement extends ChoiceElement {
	constructor() {
		super();

		this.type = 'audio-select';
		this.settings.multiAnswer = false;

		this.document.form.classList += ' form-audio-select';
		this.document.div = document.createElement('div');
		this.document.div.className = 'form-input';
		this.document.options = [];

		this.document.answer.appendChild(this.document.div);

		this.document.answer.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setChoices(choices) {
		super.setChoices(choices);

		this.document.options = [];
		this.document.div.innerHTML = '';

		for(let i = 0; i < choices.length; i++) {
			let choice = choices[i];
			let option = document.createElement('label');
			let input = document.createElement('input');
			let audio = document.createElement('audio');

			input.name = this.name;
			input.value = choice.getId();

			if(this.settings.multiAnswer)
				input.type = 'checkbox';
			else
				input.type = 'radio';

			audio.setAttribute('controls', 'controls');
			audio.setAttribute('src', choice.getAudioURL());
			option.appendChild(input);
			option.appendChild(audio);
			option.setAttribute('data-index', i);

			this.document.options.push(option);
			this.document.div.appendChild(option);
		}
	}

	setMultiAnswer(multiAnswer) {
		if(this.settings.multiAnswer != multiAnswer) {
			super.setMultiAnswer(multiAnswer);

			for(var i = 0; i < this.document.options.length; i++) {
				let option = this.document.options[i];

				let input = option.firstChild;

				if(this.settings.multiAnswer)
					input.type = 'checkbox';
				else
					input.type = 'radio';
			}
		}
	}

	addChoice(choice) {
		super.addChoice(choice);

		let choice = choices[i];
		let option = document.createElement('label');
		let input = document.createElement('input');
		let audio = document.createElement('audio');

		input.name = this.name;
		input.value = choice.getId();

		if(this.settings.multiAnswer)
			input.type = 'checkbox';
		else
			input.type = 'radio';

		audio.setAttribute('controls', 'controls');
		audio.setAttribute('src', choice.getAudioURL());
		option.appendChild(input);
		option.appendChild(audio);
		option.setAttribute('data-index', i);

		this.document.options.push(option);
		this.document.div.appendChild(option);
	}

	removeChoice(index) {
		super.removeChoice(index);

		this.document.options.splice(index, 1);
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let data_index = parseInt(option.getAttribute('data-index'));

			if(index == data_index) {
				option.remove();
			}
		}

		// resort data-index
		options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			option.setAttribute('data-index', i);
		}
	}

	getSelectedItems() {
		let items = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				items.push(option);
			}
		}

		return items;
	}

	getAnswers() {
		let answers = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				answers.push(input.value);
			}
		}

		return answers;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}

class VideoSelectElement extends ChoiceElement {
	constructor() {
		super();

		this.type = 'video-select';
		this.settings.multiAnswer = false;

		this.document.form.classList += ' form-video-select';
		this.document.div = document.createElement('div');
		this.document.div.className = 'form-input';
		this.document.options = [];

		this.document.answer.appendChild(this.document.div);

		this.document.answer.addEventListener('focusin', event => {
			// focus in event
		});
	}

	setChoices(choices) {
		super.setChoices(choices);

		this.document.options = [];
		this.document.div.innerHTML = '';

		for(let i = 0; i < choices.length; i++) {
			let choice = choices[i];
			let option = document.createElement('label');
			let input = document.createElement('input');
			let video = document.createElement('video');

			input.name = this.name;
			input.value = choice.getId();

			if(this.settings.multiAnswer)
				input.type = 'checkbox';
			else
				input.type = 'radio';

			video.setAttribute('controls', 'controls');
			video.setAttribute('src', choice.getVideoURL());
			option.appendChild(input);
			option.appendChild(video);
			option.setAttribute('data-index', i);

			this.document.options.push(option);
			this.document.div.appendChild(option);
		}
	}

	addChoice(choice) {
		super.addChoice(choice);

		let choice = choices[i];
		let option = document.createElement('label');
		let input = document.createElement('input');
		let video = document.createElement('video');

		input.name = this.name;
		input.value = choice.getId();

		if(this.settings.multiAnswer)
			input.type = 'checkbox';
		else
			input.type = 'radio';

		video.setAttribute('controls', 'controls');
		video.setAttribute('src', choice.getVideoURL());
		option.appendChild(input);
		option.appendChild(video);
		option.setAttribute('data-index', i);

		this.document.options.push(option);
		this.document.div.appendChild(option);
	}

	removeChoice(index) {
		super.removeChoice(index);

		this.document.options.splice(index, 1);
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let data_index = parseInt(option.getAttribute('data-index'));

			if(index == data_index) {
				option.remove();
			}
		}

		// resort data-index
		options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			option.setAttribute('data-index', i);
		}
	}

	getSelectedItems() {
		let items = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				items.push(option);
			}
		}

		return items;
	}

	getAnswers() {
		let answers = [];
		let options = this.document.div.getElementsByTagName('label');

		for(var i = 0; i < options.length; i++) {
			let option = options[i];
			let input = option.firstChild;

			if(input.checked) {
				answers.push(input.value);
			}
		}

		return answers;
	}

	render(parent) {
		parent.appendChild(this.document.form);
	}
}